package ru.t1.nikitushkina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO>
        implements ITaskDTORepository {

    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final TaskDTO task = new TaskDTO(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final TaskDTO task = new TaskDTO(name);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName()
                + " m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    protected Class<TaskDTO> getClazz() {
        return TaskDTO.class;
    }

}
