package ru.t1.nikitushkina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.repository.model.ISessionRepository;
import ru.t1.nikitushkina.tm.model.Session;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractUserOwnedRepository<Session>
        implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<Session> getClazz() {
        return Session.class;
    }

}
