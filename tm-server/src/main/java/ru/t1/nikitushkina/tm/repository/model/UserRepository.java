package ru.t1.nikitushkina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.model.IUserRepository;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.model.User;

import javax.persistence.EntityManager;

public class UserRepository extends AbstractRepository<User>
        implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        @NotNull final User user = create(login, password);
        user.setEmail((email == null) ? "" : email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email,
            @NotNull final String lastName,
            @NotNull final String firstName,
            @Nullable final String middleName
    ) {
        @NotNull final User user = create(login, password);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        if (middleName != null) user.setMiddleName(middleName);
        if (email != null) user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.email = :email";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.login = :login";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected Class<User> getClazz() {
        return User.class;
    }

    @Override
    public Boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @Override
    public Boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

}
