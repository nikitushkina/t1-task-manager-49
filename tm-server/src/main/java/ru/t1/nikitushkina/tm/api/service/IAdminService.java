package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IAdminService {

    void dropScheme(@Nullable String initToken);

    void initScheme(@Nullable String initToken);

}
