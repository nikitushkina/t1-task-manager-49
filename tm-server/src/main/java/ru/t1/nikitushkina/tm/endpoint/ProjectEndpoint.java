package ru.t1.nikitushkina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.endpoint.IProjectEndpoint;
import ru.t1.nikitushkina.tm.api.service.IServiceLocator;
import ru.t1.nikitushkina.tm.api.service.dto.IProjectDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;
import ru.t1.nikitushkina.tm.dto.request.*;
import ru.t1.nikitushkina.tm.dto.response.*;
import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.exception.EndpointException;
import ru.t1.nikitushkina.tm.exception.entity.ProjectNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.nikitushkina.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getProjectId();
        @Nullable final Status status = request.getStatus();
        try {
            getProjectService().changeProjectStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            getProjectService().clear(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getProjectId();
        try {
            getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            @NotNull final ProjectDTO project = getProjectService().create(userId, name, description);
            return new ProjectCreateResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    private IProjectDTOService getProjectService() {
        return this.getServiceLocator().getProjectService();
    }

    @NotNull
    private IProjectTaskDTOService getProjectTaskService() {
        return this.getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        try {
            @NotNull final List<ProjectDTO> projects = getProjectService().findAll(userId, sort);
            return new ProjectListResponse(projects);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getProjectId();
        @Nullable ProjectDTO project;
        try {
            project = getProjectService().findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        if (project == null) throw new ProjectNotFoundException();
        try {
            getProjectTaskService().removeProjectById(userId, project.getId());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIdResponse showById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getProjectId();
        try {
            @Nullable final ProjectDTO project = getProjectService().findOneById(userId, id);
            return new ProjectShowByIdResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getProjectId();
        try {
            getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            getProjectService().updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectUpdateByIdResponse();
    }

}
