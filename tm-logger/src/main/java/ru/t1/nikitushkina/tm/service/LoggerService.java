package ru.t1.nikitushkina.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    private final ObjectMapper objectMapper = new YAMLMapper();

    @SneakyThrows
    public void log(final String text) {
        final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        final String table = event.get("table").toString();
        final byte[] bytes = text.getBytes();
        final String fileExt = ".yaml";
        final String fileName = table + fileExt;
        final File file = new File(fileName);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(fileName), bytes, StandardOpenOption.APPEND);
    }

}
