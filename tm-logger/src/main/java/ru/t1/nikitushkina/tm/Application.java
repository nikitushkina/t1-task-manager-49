package ru.t1.nikitushkina.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.component.Bootstrap;

public final class Application {

    public static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    public static final String QUEUE = "LOGGER";

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.startLogger();
    }

}
